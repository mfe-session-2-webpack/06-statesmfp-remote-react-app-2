import React from 'react';

import BuyButtons from './buy-buttons/BuyButtons';

function App() {
  return (
    <div className="App">
      <BuyButtons />
    </div>
  );
}

export default App;
