import React from 'react';

import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';

export default function BuyButtons() {
  function publishEvent(data) {
    const event = new CustomEvent('cartAction', { detail: data });
    document.dispatchEvent(event);
  }

  return (
    <Box sx={{ '& button': { m: 1 } }}>
      <Typography variant="h4" gutterBottom>
        Add or Remove products to cart
      </Typography>
      <div>
        <Button
          onClick={() => publishEvent('add')}
          variant="contained"
          size="medium"
        >
          Add
        </Button>
        <Button
          onClick={() => publishEvent()}
          variant="contained"
          size="medium"
        >
          Remove
        </Button>
      </div>
    </Box>
  );
}
